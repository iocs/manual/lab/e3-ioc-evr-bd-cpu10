
require mrfioc2, 2.3.1+6

epicsEnvSet "TOP" "$(E3_CMD_TOP)/"

iocshLoad "$(TOP)/iocrc.iocsh"

# [IOC] core
iocshLoad "$(mrfioc2_DIR)/evr.iocsh"      "P=$(PEVR),PCIID=$(PCIID),EVRDB=$(EVRDB=evr-mtca-300-univ.db)"
iocshLoad "$(mrfioc2_DIR)/evrevt.iocsh"   "P=$(PEVR),$(EVREVTARGS=)"
epicsThreadSleep (1)

# nBLM & icBLM database
dbLoadRecords("$(TOP)/xBLM.db") "P=$(PEVR)"

iocInit

iocshLoad "$(mrfioc2_DIR)/evr.r.iocsh" "P=$(PEVR), INTREF=$(INTREF=)"
iocshLoad "$(mrfioc2_DIR)/evrtclk.r.iocsh" "P=$(PEVR)"
iocshLoad "$(mrfioc2_DIR)/evrdlygen.r.iocsh" "P=$(PEVR),$(EVRDLYGENARGS=)"
iocshLoad "$(mrfioc2_DIR)/evrout.r.iocsh" "P=$(PEVR),$(EVROUTARGS=)"
iocshLoad "$(mrfioc2_DIR)/evrin.r.iocsh" "P=$(PEVR),$(EVRINARGS=)"
iocshLoad "$(TOP)evrLb.r.iocsh" "P=$(PEVR)"

#In case of problems, execute this:
#iocshLoad "$(TOP)sequencer_fix.iocsh" "P=$(PEVR)"
