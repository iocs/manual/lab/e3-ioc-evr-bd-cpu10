# nBLM/icBLM Systems loopback mode

* The scritp sets EVR in loopack mode and generates clock in uTCA backplane. icBLM system uses the EVR clock as clock source 
for data acquisition.

* Use EVR firmware 207.7 for precise loop-back mode.

* Use command 'camonitor -tIs $(P)$(R=)$(S=:)TstF14Hz' to check if EVR triggers are being generated.

* In case of failure, write value 150 to PV '$(P)$(R=)$(S=:)Link-Clk-SP', wait 2 s and write it back to original value 88.025
